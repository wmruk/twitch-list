var channels = ["ESL_SC2", "thecoppinger", "mljware", "freecodecamp", "xopherdeep", "joelpurra", "RobotCaleb", "noobs2ninjas"];

function getStream(name, t_data){
	var url = 'https://wind-bow.gomix.me/twitch-api/streams/' + name + '?callback=?';
	 $.getJSON(url, function(data) {
		var html = ' <div class="media text-muted pt-3">';
		html+='<img alt="32x32" class="mr-2 rounded" style="width: 32px; height: 32px;" src="'+t_data.logo+'" data-holder-rendered="true">';
		html+='<p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">';
		html+='<a href="'+t_data.url+'"><strong class="d-block text-gray-dark">'+t_data.name+'</strong></a>';
		html+=t_data.status;
		html+='</p>';
		html+='</div>'
					 	
		if (data.stream == null){
			$('#offline').append(html);
		}else{
			$('#online').append(html);
		}
    });
}

function getChannel(name){
	var url = 'https://wind-bow.gomix.me/twitch-api/channels/' + name + '?callback=?';

	$.getJSON(url, function(data) {
		getStream(name, data);
      });
}

function getChannelInfo() {
  channels.forEach(function(channel) {
  	getChannel(channel);
  });
};

$(document).ready(function() {
  getChannelInfo();
});
